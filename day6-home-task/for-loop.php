<?php

// for loop

for ($x = 0; $x <= 5; $x++){
    echo "The number is: $x <br>";
}

echo "<br>";
echo "<br>";

for ($i = 1; ;$i++){
    if ($i > 10){
        break;
    }
    echo $i . "<br>";
}

echo "<br>";
echo "<br>";

$j = 1;

for (; ;){
    if ($j > 10){
        break;
    }
    echo $j . "<br>";
    $j++;
}

echo "<br>";
echo "<br>";

for ($k = 1, $l = 0; $k <= 10; $l += $k, print $k, $k++);

echo "<br>";
echo "<br>";

for ($x = 0; $x <= 10; $x++){
    echo "This is a number - " . $x ."<br>";
}


?>