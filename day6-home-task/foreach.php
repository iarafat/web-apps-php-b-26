<?php

// foreach

$color = array("red", "green", "blue", "yellow");

foreach ($color as $value){
    echo $value . "<br>";
}

echo "<br>";
echo "<br>";

$arr = array(1, 2, 3, 4, 5);

foreach ($arr as &$value){
    $value = $value * 2;
}

foreach ($arr as $key => $value){
    echo "{$key} => {$value}";
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

?>