<?php

// switch
$favcolor = "blue";

switch ($favcolor){
    case "red":
        echo "Your favorite color is red!";
        break;
    case "blue":
        echo "Your favorite color is blue!";
        break;
    case "green":
        echo "Your favorite color is green!";
        break;
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}
echo "<br>";
echo "<br>";

$i = 3;

switch ($i){
    case 0:
    case 1:
    case 2:
        echo "i is less than 3 but not negative";
        break;
    case 3;
        echo "i is 3";
}
echo "<br>";
echo "<br>";

$t = "abc";

switch ($t){
    case 'bcd';
    case 'dcb';
    case 'abc';
        echo "Good choice";
        break;
    default;
        echo "Please make a new selection";
        break;
}

?>