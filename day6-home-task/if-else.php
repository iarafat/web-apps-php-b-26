<?php

// if
$t = date("H");

if ($t < "20"){
   echo "Have a good day!";
    echo "<br>";
    echo "<br>";
}

// if else
if ($t < "10"){
    echo "Have a good day!";
}
else {
    echo "Have a good night!";
}
echo "<br>";
echo "<br>";

// if elseif else
if ($t < "10"){
    echo "Have a good morning";
}elseif ($t < "10"){
    echo "Have a good day";
}elseif ($t < "12"){
    echo "Have a good afternoon";
}else {
    echo "Have a good night";
}


?>