<?php

namespace App\GenderSelection;

class GenderSelection
{
    public $id = "";
    public $title = "";
    public $gender = "";
    public $data = "";
    public $email = "";

    public function prepare($data = "")
    {
                
        if (!empty($data["id"])) {
            $this->id = $data["id"];
        }
        if (!empty($data["name"])) {
            $this->title = $data["name"];
        } else {
            $_SESSION['N_Mes'] = "Name required";
        }
        if (!empty($data["email"])) {
            $this->email = $data["email"];
        } else {
            $_SESSION['E_Mes'] = "Email required";
        }
        if (!empty($data["gender"])) {
            $this->gender = $data["gender"];
        } else {
            $_SESSION['G_Mes'] = "Gender selection is required";
        }
        
//        $_SESSION['Form_Data'] = $data;
        
        return $this;
    }

    public function __construct()
    {
        session_start();
        $conn = mysql_connect("localhost", "root", "") or die('Opps! Unable to connect with Mysql');
        mysql_select_db("php26") or die('Unable to connect with Database');
    }

    public function store()
    {
        if (!empty($this->title) && !empty($this->email) && !empty($this->gender)) {

            $query = "INSERT INTO `gender_selection` (`id`, `title`, `gender`, `unique_id`,`email`) VALUES (NULL, '$this->title',
'$this->gender', '" . uniqid() . "', '$this->email')";

            if (mysql_query($query)) {
                $_SESSION["message"] = "Data Successfully Submitted";
//                header("location:index.php");
            }
        } else {
//            header("location:index.php");

            $_SESSION['T_Value'] = "$this->title";
            $_SESSION['E_Value'] = "$this->email";
            $_SESSION['G_Value'] = "$this->gender";
        }
        header("location:index.php");

    }

    public function ValidationMessage($sm="")
    {
        if (isset($_SESSION["$sm"]) && !empty($_SESSION["$sm"])) {
            echo $_SESSION["$sm"];
            unset($_SESSION["$sm"]);
        }
    }

    public function index()
    {
        $query = "SELECT * FROM `gender_selection`";
        $mydata = mysql_query($query);
        while ($row = mysql_fetch_assoc($mydata)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function show()
    {
        $query = "SELECT * FROM `gender_selection` WHERE unique_id=" . "'" . $this->id . "'";
//        echo $query;
//        die();
        $mydata = mysql_query($query);
        $row = mysql_fetch_assoc($mydata);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `gender_selection` SET `title` = '$this->title', `email` = '$this->email',  `gender` = '$this->gender'
WHERE
        `gender_selection`.`unique_id` =" . "'" . $this->id . "'";
//        echo $query;
//        die();
        if (mysql_query($query)) {
            $_SESSION["message"] = "Data Successfully Updated";
        }
        header("location:edit.php?id=$this->id");
    }

    public function delete()
    {
        $query = "DELETE FROM `gender_selection` WHERE `gender_selection`.`unique_id` =" . "'" . $this->id . "'";
        if (mysql_query($query)) {
            $_SESSION["message"] = "Data Successfully Deleted";
        }
        header("location:process.php");
    }


}