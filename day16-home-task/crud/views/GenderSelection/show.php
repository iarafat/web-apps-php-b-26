<?php
include_once "../../vendor/autoload.php";

use App\GenderSelection\GenderSelection;

$showObject = new GenderSelection();

$onedata = $showObject->prepare($_GET)->show();

//echo "<pre>";
//print_r($onedata);


if (isset($onedata) && !empty($onedata)) {

    ?>

    <html>
    <head>
        <title>Edit | Show</title>
        <link rel="stylesheet" href="../../vendor/twitter/bootstrap/dist/css/bootstrap.min.css">
        <script src="jquery.min.js"></script>
        <script src="../../vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h3><a href="create.php">Back to add page</a> | <a href="index.php">Back to list page</a></h3>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Unique ID</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $onedata['id'] ?></td>
                        <td><?php echo $onedata['title'] ?></td>
                        <td><?php echo $onedata['email'] ?></td>
                        <td><?php echo $onedata['gender'] ?></td>
                        <td><?php echo $onedata['unique_id'] ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
    </html>


<?php } else {
    $_SESSION["Err_Mes"] = "Not Found, Something going wrong !" . "<a href='create.php'>Create</a>";
    header("location:errors.php");
}
?>