<?php
include_once "../../vendor/autoload.php";

use App\GenderSelection\GenderSelection;

$editObject = new GenderSelection();

$singledata = $editObject->prepare($_GET)->show();
//
//echo "<pre>";
//print_r($singledata);

if (isset($singledata) && !empty($singledata)) {

    ?>


    <html>
    <head>
        <title>Edit Page</title>
        <link rel="stylesheet" href="../../vendor/twitter/bootstrap/dist/css/bootstrap.min.css">
        <script src="jquery.min.js"></script>
        <script src="../../vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">

                <h3><a href="index.php">See list</a></h3>
                <strong class="text-success"><?php $editObject->ValidationMessage("message"); ?></strong>

                <fieldset style="margin-top: 50px">
                    <legend>Update Gender Selection</legend>
                    <form action="update.php" method="post">
                        <label>Name: </label>
                        <input type="text" class="form-control" name="name" placeholder="Update your name" value="<?php echo $singledata["title"]
                        ?>"><br>
                        <label>Email: </label>
                        <input type="text" class="form-control" name="email" placeholder="Update your email" value="<?php echo $singledata["email"]
                        ?>"><br>
                        <label>Update your gender: </label>
                        <label for="male">
                            <input type="radio" name="gender"
                                   value="Male" <?php echo ($singledata["gender"] == 'Male') ? 'checked' : '' ?> id="male">
                            Male
                        </label>
                        <label for="female">
                            <input type="radio" name="gender"
                                   value="Female" <?php echo ($singledata["gender"] == 'Female') ? 'checked' : '' ?> id="female">
                            Female
                        </label><br><br>
                        <input type="hidden" name="id" value="<?php echo $_GET["id"] ?>">
                        <input type="submit" value="Update" class="btn btn-primary">
                    </form>
                </fieldset>

            </div>
        </div>
    </div>
    </body>
    </html>


<?php } else {
    $_SESSION["Err_Mes"] = "Not Found, Something going wrong !" . "<a href='create.php'>Create</a>";
    header("location:errors.php");
}
?>