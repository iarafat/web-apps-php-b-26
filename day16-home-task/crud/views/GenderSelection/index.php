<?php
include_once "../../vendor/autoload.php";

use App\GenderSelection\GenderSelection;

$listObject = new GenderSelection();

$alldata = $listObject->index();

//echo "<pre>";
//print_r($alldata);

?>
<html>
<head>
    <title>Index | List</title>
    <link rel="stylesheet" href="../../vendor/twitter/bootstrap/dist/css/bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="../../vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <strong class="text-success"><?php $listObject->ValidationMessage("message"); ?></strong>

            <h3><a href="create.php">Back to add page</a> | Download as <a href="pdf.php">PDF</a> And <a href="xl.php">XL</a></h3>
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th colspan="3">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial = 1;
                if (isset($alldata) && !empty($alldata)) {
                    foreach ($alldata as $onedata) {
                        ?>

                        <tr>
                            <td><?php echo $serial++ ?></td>
                            <td><?php echo $onedata["title"] ?></td>
                            <td><?php echo $onedata["email"] ?></td>
                            <td><?php echo $onedata["gender"] ?></td>
                            <td><a href="show.php?id=<?php echo $onedata["unique_id"] ?>">View</a></td>
                            <td><a href="edit.php?id=<?php echo $onedata["unique_id"] ?>">Edit</a></td>
                            <td><a href="delete.php?id=<?php echo $onedata["unique_id"] ?>">Delete</a></td>
                        </tr>

                    <?php }
                } else { ?>
                    <tr>
                        <td colspan="4">No available Data</td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>

