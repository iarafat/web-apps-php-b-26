<?php
session_start();
?>
<html>
<head>
    <title>Index | List</title>
</head>
<body>
<a href="create2.php">Back to add page</a>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Email</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($_SESSION['All_Data']) && !empty($_SESSION['All_Data'])) {
        foreach ($_SESSION['All_Data'] as $kay=>$onedata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php if (isset($onedata["name"])){ echo $onedata["name"];} ?></td>
                <td><?php if (isset($onedata["email"])){ echo $onedata["email"];} ?></td>
                <td><a href="show2.php?id=<?php echo $kay ?>">View</a></td>
                <td><a href="edit2.php?id=<?php echo $kay ?>">Edit</a></td>
                <td><a href="delete2.php?id=<?php echo $kay ?>">Delete</a></td>
            </tr>

        <?php }
    } else { ?>
        <tr>
            <td colspan="4">No available Data</td>
        </tr>
    <?php } ?>

</table>
</body>
</html>

