<?php

include_once "../../vendor/autoload.php";

use App\GenderSelection\GenderSelection;

$obj = new GenderSelection();


//if (isset($_SESSION["Form_Data"]) && !empty($_SESSION["Form_Data"])) {
//    echo $_SESSION["message"];
//    unset($_SESSION["message"]);
//}


?>

<html>
<head>
    <title>Add Page</title>
    <link rel="stylesheet" href="../../vendor/twitter/bootstrap/dist/css/bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="../../vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <h3><a href="index.php">See list</a></h3>
            <strong class="text-success"><?php $obj->ValidationMessage("message"); ?></strong>

            <fieldset style="margin-top: 50px">
                <legend>Gender Selection</legend>
                <form action="store.php" method="post">
                    <label>Name: </label>
                    <input type="text" class="form-control" name="name" placeholder="Enter your name" value="<?php
                    $obj->ValidationMessage("T_Value");

                    //        if (isset($_SESSION["Form_Data"]['name']) && !empty($_SESSION["Form_Data"]['name'])) {
                    //            echo $_SESSION["Form_Data"]['name'];
                    //            unset($_SESSION["Form_Data"]['name']);
                    //        }
                    ?>">
                    <strong class="text-danger"><?php $obj->ValidationMessage("N_Mes") ?></strong>
                    <br>
                    <label>Email: </label>
                    <input type="text" class="form-control" name="email" placeholder="Enter your email" value="<?php
                    $obj->ValidationMessage("E_Value");

                    //        if (isset($_SESSION["Form_Data"]['email']) && !empty($_SESSION["Form_Data"]['email'])) {
                    //            echo $_SESSION["Form_Data"]['email'];
                    //            unset($_SESSION["Form_Data"]['email']);
                    //        }
                    ?>">
                    <strong class="text-danger"><?php $obj->ValidationMessage("E_Mes") ?></strong>
                    <br>
                    <label>Select your gender: </label>
                    <label for="male">
                        <input type="radio" name="gender" value="Male" id="male">
                        Male
                    </label>
                    <label for="female">
                        <input type="radio" name="gender" value="Female" id="female">
                        Female
                    </label>
                    <br>
                    <?php
                    $obj->ValidationMessage("G_Value")

                    //        if (isset($_SESSION["Form_Data"]['gender']) && !empty($_SESSION["Form_Data"]['gender'])) {
                    //            echo $_SESSION["Form_Data"]['gender'];
                    //            unset($_SESSION["Form_Data"]['gender']);
                    //        }
                    ?>
                    <strong class="text-danger"><?php $obj->ValidationMessage("G_Mes") ?></strong>
                    <br><br>
                    <input type="submit" class="btn btn-primary">
                </form>
            </fieldset>
        </div>
    </div>
</div>
</body>
</html>

