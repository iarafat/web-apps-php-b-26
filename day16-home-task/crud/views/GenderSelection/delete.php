<?php
include_once "../../vendor/autoload.php";

use App\GenderSelection\GenderSelection;

$deleteObject = new GenderSelection();

$deleteObject->prepare($_GET)->delete();

?>