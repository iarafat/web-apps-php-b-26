$(document).ready(function() {
    $("#submit").click(function() {
        var name = $("#name").val();
        var email = $("#email").val();
        var gender = $("input[type=radio]:checked").val();
        if (name == '' || email == '' || gender == '') {
            alert("Insertion Failed Some Fields are Blank....!!");
        } else {
// Returns successful data submission message when the entered information is stored in database.
            $.post("store2.php", {
                name1: name,
                email1: email,
                gender1: gender
            }, function(data) {
                alert(data);
                $('#form')[0].reset(); // To reset form fields
            });
        }
    });
});