<?php
include("../../vendor/mpdf/mpdf/mpdf.php");
include_once "../../vendor/autoload.php";

use App\GenderSelection\GenderSelection;

$obj = new GenderSelection();

$alldata = $obj->index();

$trs = "";
$serial = 0;
foreach ($alldata as $data):
//    echo "<pre>";
//    print_r($data);

    $serial++;
    $trs .= "<tr>";
    $trs .= "<td>" . $serial . "</td>";
    $trs .= "<td>" . $data['id'] . "</td>";
    $trs .= "<td>" . $data['title'] . "</td>";
    $trs .= "<td>" . $data['email'] . "</td>";
    $trs .= "<td>" . $data['gender'] . "</td>";
    $trs .= "<td>" . $data['unique_id'] . "</td>";

endforeach;

$html = <<<EOD
<!doctype html>
<head>
<title>List of gender entered data</title>
    <link rel="stylesheet" href="../../vendor/twitter/bootstrap/dist/css/bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="../../vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<html>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <h1>List of gender entered data</h1>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SL.</th>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Unique ID</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                $trs;
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
</body>
</html>
EOD;

$mpdf = new mPDF();

$mpdf->WriteHTML($html);

$mpdf->Output();

exit;

?>
