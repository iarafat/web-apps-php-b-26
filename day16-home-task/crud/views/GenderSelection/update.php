<?php
include_once "../../vendor/autoload.php";

use App\GenderSelection\GenderSelection;

$updateObject = new GenderSelection();

$updateObject->prepare($_POST)->update();
?>