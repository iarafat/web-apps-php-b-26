<?php
include_once "../../vendor/autoload.php";

use App\EmailSubscription\EmailSubscription;

$obj = new EmailSubscription();

$alldata = $obj->index();

?>

<html>
<body>
<a href="index.php">List Page</a>
<a href="create.php">Add Page</a>


<table border="1" cellpadding="10">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th>Email</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $sl = 1;
    foreach ($alldata as $onedata) {
        ?>

        <tr>
            <td><?php echo $sl++ ?></td>
            <td><?php echo $onedata["title"] ?></td>
            <td><?php echo $onedata["email"] ?></td>
            <td><a href="show.php?id=<?php echo $onedata["unique_id"] ?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $onedata["unique_id"] ?>">Edit</a></td>
            <td><a href="delete.php?id=<?php echo $onedata["unique_id"] ?>">Delete</a></td>
        </tr>

    <?php } ?>

</table>
</body>
</html>
