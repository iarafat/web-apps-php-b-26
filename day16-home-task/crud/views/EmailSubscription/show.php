<?php
include_once "../../vendor/autoload.php";

use App\EmailSubscription\EmailSubscription;

$obj = new EmailSubscription();

//$alldata = $obj->s();

//print_r($_GET);

$obj->prepare($_GET);
$onedata = $obj->show();

//print_r($onedata);
?>

<html>
<body>
<table border="1" cellpadding="10">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Email</th>
        <th>Unique ID</th>
    </tr>
    <tr>
        <td><?php echo $onedata["id"] ?></td>
        <td><?php echo $onedata["title"] ?></td>
        <td><?php echo $onedata["email"] ?></td>
        <td><?php echo $onedata["unique_id"] ?></td>
    </tr>
</table>
</body>
</html>
