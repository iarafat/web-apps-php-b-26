<?php
session_start();

if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}

?>

<html>
<body>
<a href="index.php">List Page</a>
<a href="create.php">Add Page</a>

<fieldset style="width:30%; display: block; margin: 50px auto">
    <legend>Email Subscription</legend>
    <form action="store.php" method="post">
        <input type="text" name="subject" placeholder="subject">
        <input type="email" name="email" placeholder="email">
        <input type="submit">
    </form>
</fieldset>

</body>
</html>
