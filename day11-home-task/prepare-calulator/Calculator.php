<?php

class Calculator
{
    public $number1 = "";
    public $number2 = "";
    
    public function prepare($data){
        echo "<pre>";
        print_r($data);
        
        $this->number1 = $data["number1"];
        $this->number2 = $data["number2"];
        return $this;
    }

    public function add(){
        echo "Addition is:".($this->number1 + $this->number2)."<br>";
        return $this;
    }

    public function sub(){
        echo "Subtraction is:".($this->number1 - $this->number2);
    }
}