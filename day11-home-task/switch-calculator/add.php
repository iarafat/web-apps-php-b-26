<?php


//include_once "Calculator.php";

function __autoload($classname){
    $filename = "./".$classname.".php";
    include_once "$filename";
}

$c = new Calculator();
$c->prepare($_POST["number1"], $_POST["number2"]);

$name= $_POST["all"];

switch ($name){
    case "+":
        $c->addition();
        break;
    case "-":
        $c->substruction();
        break;
    case "*":
        $c->multiplication();
        break;
    case "/":
        $c->division();
        break;
    default:
        echo "Input invalid";
}

?>