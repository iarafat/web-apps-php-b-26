<?php
include_once "../../src/FavoritActor/FavoritActor.php";

$editObject = new FavoritActor();

$singledata = $editObject->prepare($_GET)->show();

if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}

//foreach ($singledata as $item) {
//    $selected = "";
//    if ($item == $singledata)
//        $selected = "selected";
//}
//
//?>

<html>
<head>
    <title>Edit | Edit page</title>
</head>
<body>
<!--<fieldset style="width: 30%">-->
<!--    <legend>Update Your Favorite Actor</legend>-->
<!--    <form action="update.php" method="post" id="factors">-->
<!--        <select name="factor" form="factors">-->
<!--            <option value="--><?php //echo $item; ?><!--" selected="--><?php //echo $selected; ?><!--">--><?php //echo $item; ?><!--</option>-->
<!--            <option value="Jack Nicholson">Jack Nicholson</option>-->
<!--            <option value="Marlon Brando">Marlon Brando</option>-->
<!--            <option value="Robert De Niro">Robert De Niro</option>-->
<!--            <option value="Al Pacino">Al Pacino</option>-->
<!--            <option value="Daniel Day-Lewis">Daniel Day-Lewis</option>-->
<!--        </select>-->
<!--        <input type="hidden" name="id" value="--><?php //echo $_GET["id"] ?><!--">-->
<!--        <input type="submit" value="update">-->
<!--    </form>-->
<!--</fieldset>-->

<fieldset style="width: 30%">
    <legend>Update Your Favorite Actor</legend>
    <form action="update.php" method="post" id="factors">
        <select name="factor" form="factors">
            <option value="<?php echo $singledata["title"] ?>"><?php echo $singledata["title"] ?></option>
            <option value="Jack Nicholson">Jack Nicholson</option>
            <option value="Marlon Brando">Marlon Brando</option>
            <option value="Robert De Niro">Robert De Niro</option>
            <option value="Al Pacino">Al Pacino</option>
            <option value="Daniel Day-Lewis">Daniel Day-Lewis</option>
        </select>
        <input type="hidden" name="id" value="<?php echo $_GET["id"] ?>">
        <input type="submit" value="update">
    </form>
</fieldset>



</body>
</html>

