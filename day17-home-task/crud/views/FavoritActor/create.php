<?php
session_start();
if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}

?>

<html>
<head>
    <title>Create | Add page</title>
</head>
<body>
<fieldset style="width: 30%">
    <legend>Select Your Favorite Actor</legend>
    <form action="store.php" method="post" id="factors">
        <select name="factor" form="factors">
            <option  selected disabled hidden>Choice here</option>
            <option value="Jack Nicholson">Jack Nicholson</option>
            <option value="Marlon Brando">Marlon Brando</option>
            <option value="Robert De Niro">Robert De Niro</option>
            <option value="Al Pacino">Al Pacino</option>
            <option value="Daniel Day-Lewis">Daniel Day-Lewis</option>
        </select>
        <input type="submit">
    </form>
</fieldset>
</body>
</html>
