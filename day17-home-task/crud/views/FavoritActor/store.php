<?php
include_once "../../src/FavoritActor/FavoritActor.php";

$storeObject = new FavoritActor();

$storeObject->prepare($_POST);

if (isset($_POST['factor']) && !empty($_POST['factor'])){
    $storeObject->store();
} else {
    echo "Please try again...";
}

?>