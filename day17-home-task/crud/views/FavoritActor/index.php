<?php
include_once "../../src/FavoritActor/FavoritActor.php";

$listObject = new FavoritActor();

$alldata = $listObject->index();
//echo "<pre>";
//print_r($alldata);


if (isset($_SESSION["message"]) && !empty($_SESSION["message"])) {
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}

?>

<html>
<head>
    <title>Index | List page</title>
</head>
<body>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Favorite Actor Name</th>
            <th colspan="3">Action</th>
        </tr>
        <?php
        $serial = 1;

        if (isset($alldata) && !empty($alldata)){
            foreach ($alldata as $onedata){ ?>

                <tr>
                    <td><?php echo $serial++ ?></td>
                    <td><?php echo $onedata["title"]?></td>
                    <td><a href="show.php?id=<?php echo $onedata["id"]?>">View</a></td>
                    <td><a href="edit.php?id=<?php echo $onedata["id"]?>">Edit</a></td>
                    <td><a href="delete.php?id=<?php echo $onedata["id"]?>">Delete</a></td>
                </tr>

        <?php } } else { ?>

            <tr>
                <td colspan="3">No available Data</td>
            </tr>


        <?php } ?>

    </table>
</body>
</html>
