<?php
namespace MySampleNS;
echo "the namespace is - ".__NAMESPACE__;
echo "<br>";

echo __LINE__;
echo "<br>";

echo __FILE__;
echo "<br>";

echo __DIR__;
echo "<br>";

function fname(){
    echo "Function name is - ";
    echo __FUNCTION__;
}
fname();
echo "<br>";

class classname {
    public function __construct(){

        echo __CLASS__;
    }
}
$obj = new classname();
echo "<br>";

class Sample {
    public static function myMethod(){
        echo "the name of method is - " . __METHOD__;
    }
}
Sample::myMethod();
echo "<br>";







?>