<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form Handling</title>
</head>
<body>
<fieldset>
    <legend>Calculator - Division</legend>
    <form action="calculator.php" method="post">
        Number 1: <input type="number" name="number1">
        Number 2: <input type="number" name="number2">
        <button type="submit">Submit</button>
    </form>
</fieldset>
</body>
</html>