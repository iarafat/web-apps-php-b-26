<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form Handling</title>
</head>
<body>

<form action="add.php" method="post">
    First Name: <input type="text" name="first_name"><br>
    Last Name: <input type="text" name="last_name"><br>
    Phone 1: <input type="number" name="number1"><br>
    Phone 2: <input type="number" name="number2"><br>
    Email: <input type="email" name="email"><br>
    Address: <textarea name="address"></textarea><br>
    <button type="submit">Submit</button>
</form>

</body>
</html>