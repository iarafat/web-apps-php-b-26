<?php

// addslashes
//$str = addslashes('what "does "yolo" mean?');
//echo $str;
$str = "who's peter griffin?";
echo $str . "this is not safe in a database query. <br>";
echo addslashes($str) . " this is safe in a database query";
echo "<br>";
echo "<br>";

// explode
$sta = "hello world. it's a beautiful day.";
$ate = 'one,two,three,four';

echo "<pre>";
print_r(explode(" ", $sta));
echo "<br>";

print_r(explode(",", $ate));
echo "<br>";

print_r(explode(",", $ate,0));
echo "<br>";

print_r(explode(",", $ate,2));
echo "<br>";

print_r(explode(",", $ate,-1));
echo "</pre>";
echo "<br>";
echo "<br>";

// join & implode
$arr = array('Hello','World!','Beautiful','Day!');
echo join(" ",$arr);
echo "<br>";

echo implode(" ",$arr);
echo "<br>";
echo "<br>";

// htmlentities
$sth = "<© W3Sçh°°¦§>";
echo htmlentities($sth);
echo "<br>";
echo "<br>";

// ltrim/rtrim/trim
$st = "Hello World";
echo $st . "<br>";
echo ltrim($st, "H");
echo "<br>";

echo rtrim($st, "d");
echo "<br>";
echo "<br>";

$stt = "!Hello World!";
echo $stt."<br>";
echo trim($stt,"!");
echo "<br>";
echo "<br>";

// nl2br
echo nl2br("One line. \nAnother line.");
echo "<br>";
echo "<br>";

// str_pad/str_repeat
$str = "Hello World";
echo str_pad($str,50,".");
echo "<br>";

echo $str . str_repeat("1",15);
echo "<br>";
echo "<br>";

// str_replace
echo str_replace("world","Country","I love my world");
echo "<br>";
echo "<br>";

// str_split
$sts = "Hello World";
echo "<pre>";
print_r(str_split($sts));
echo "</pre>";
echo "<br>";
echo "<br>";

// strip_tags
$strip = "Hello <b>World</b>";
echo $strip . "<br>";
echo strip_tags($strip);
echo "<br>";
echo "<br>";

// strlen
$stl = "Hello World";
echo strlen($stl);
echo "<br>";
echo "<br>";

// strtolower/strtoupper
$sttl = "Hello World";
echo strtolower($sttl);
echo "<br>";
echo strtoupper($sttl);
echo "<br>";
echo "<br>";

// substr_count
$stc = "Hello world. The world is nice";
echo substr_count($stc,"o");
echo "<br>";
echo "<br>";

// substr_replace
$stp = "Hello world. The world is nice";
$stp2 = "Farhan";
echo substr_replace($stp,$stp2,6);
echo "<br>";
echo "<br>";

// ucfirst
$ucf = "hello world";
echo ucfirst($ucf);
?>