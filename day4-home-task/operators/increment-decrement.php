<?php



// Pre-increment
$x = 10;
echo ++$x;
echo "<br>";

// Post-increment
$x = 10;
echo $x++;
echo "<br>";

// Pre-decrement
$x = 10;
echo --$x;
echo "<br>";

// Post-decrement
$x = 10;
echo $x--;

?>