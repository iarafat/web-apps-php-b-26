<?php

// Equal
$x = 100;
$y = "100";
echo $x == $y;
echo "<br>";

var_dump($x == $y);
echo "<br>";

// Identical
var_dump($x === $y);
echo "<br>";

// Not equal
var_dump($x != $y);
echo "<br>";

var_dump($x <> $y);
echo "<br>";

// Not identical
var_dump($x !== $y);
echo "<br>";

// Greater than
$x = 100;
$y = 50;

var_dump($x > $y);
echo "<br>";

// Less than
var_dump($x < $y);
echo "<br>";

// Greater than or equal to
var_dump($x >= $y);
echo "<br>";

// Less than or equal to
var_dump($x <= $y);

?>