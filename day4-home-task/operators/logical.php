<?php

// And - &&
$x = 100;
$y = 50;

if ($x == 100  and $y == 50){
    echo "True";
}
else {
    echo "False";
}
echo "<br>";

// Or - ||
$x = 200;
$y = 50;

if ($x == 100  or $y == 80){
    echo "True";
}
else {
    echo "False";
}
echo "<br>";

// Xor
$x = 200;
$y = 50;

if ($x == 200  xor $y == 80){
    echo "True";
}
else {
    echo "False";
}
echo "<br>";

// Not
$x = 100;

if ($x !== 100){
    echo "True";
}
else {
    echo "False";
}
?>