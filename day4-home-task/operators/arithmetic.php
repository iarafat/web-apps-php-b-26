<?php

$x = 10;
$y = 6;

// Addition
echo $x + $y;
echo "<br>";

// Subtraction
echo $x - $y;
echo "<br>";

// Multiplication
echo $x * $y;
echo "<br>";

// Division
echo $x / $y;
echo "<br>";

// Modulus
echo $x % $y;
echo "<br>";

// Exponentiation
echo $x ** $y;

?>