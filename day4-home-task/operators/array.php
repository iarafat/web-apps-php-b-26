<?php

// Union
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

echo "<pre>";
print_r($x + $y);
echo "</pre>";
echo "<br>";

// Equality
$x = array("a" => "red", "b" => "green");
$y = array("a" => "red", "b" => "green");
//$y = array("c" => "blue", "d" => "yellow");

var_dump($x == $y);
echo "<br>";


// Identity
$x = array("a" => "red", "b" => "green");
$y = array("a" => "red", "b" => "green", "d" => "yellow");
//$y = array("b" => "green", "c" => "blue", "d" => "yellow");

var_dump($x === $y);
echo "<br>";

// Inequality
$x = array("a" => "red", "b" => "green");
$y = array("c" => "blue", "d" => "yellow");

var_dump($x != $y);
echo "<br>";

var_dump($x <> $y);
echo "<br>";

// Non-identity
$x = array("a" => "red", "b" => "green");
$y = array("a" => "red", "b" => "green" );
//$y = array("c" => "blue", "d" => "yellow");

var_dump($x !== $y);

?>