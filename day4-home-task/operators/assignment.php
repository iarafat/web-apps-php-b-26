<?php

$x = 20;
$y = 100;

// x = y
echo $x;
echo "<br>";

// Addition
$x = 10;
$x += 6;
echo $x;
echo "<br>";

// Subtraction
$x = 10;
$x -= 6;
echo $x;
echo "<br>";

// Multiplication
$x = 10;
$x *= 6;
echo $x;
echo "<br>";

// Division
$x = 10;
$x /= 6;
echo $x;
echo "<br>";

// Modulus
$x = 10;
$y %= 6;
echo $y;

?>