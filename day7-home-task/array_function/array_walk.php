<?php
echo "<pre>";

function myfunction($value,$key){
    echo "The key $key has the value $value<br>";
}

//$a = array("a"=>"red", "b"=>"green", "c"=>"blue", "d"=>"yellow");
$a = array("red", "green", "blue", "yellow");
array_walk($a,"myfunction");
echo "<br>";
echo "<br>";


function myf($value,$key,$parameter){
    echo "$key $parameter $value<br>";
}
$a2 = array("a"=>"red", "b"=>"green", "c"=>"blue", "d"=>"yellow");
array_walk($a2, "myf", "has the value");
echo "<br>";
echo "<br>";


function myfu(&$value,$key){
    $value="yellow";
}
$a3 = array("a"=>"red", "b"=>"green", "c"=>"blue");
array_walk($a3, "myfu");
print_r($a3);

?>