<?php

namespace App\Semester;

use PDO;

class Semester
{
    public $id = '';
    public $title = '';
    public $semester = '';
    public $offer = '';
    public $data = '';

    public $first = 9000;
    public $second = 11000;
    public $three = 13000;
    public $none = 0;
    public $no = "No";

    public $cost = '';
    public $weber = '';
    public $total = '';

    public $con = '';


    public function prepare($data = '')
    {

        // validation
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['name'])) {

            $this->title = $data['name'];

        } else {

            $_SESSION['NE_Mes'] = "Name Required *";
        }

        if (!empty($data['semester'])) {

            $this->semester = $data['semester'];

        } else {

            $_SESSION['NS_Mes'] = "Semester Required *";
        }

        if (!empty($data['offer'])) {

            $this->offer = $data['offer'];
        } else {
            $this->offer = $this->no;
        }

//        $_SESSION['AllS_Data'] = $_POST;

        // cost
        if ($this->semester == 1) {
            $this->cost = $this->first;
        } elseif ($this->semester == 2) {
            $this->cost = $this->second;
        } else {
            $this->cost = $this->three;
        }

        // weber
        if ($this->semester == 1) {
            if ($this->offer == "Yes") {
                $this->weber = $this->first * 10 / 100;
            } else {
                $this->weber = $this->none;
            }
        } elseif ($this->semester == 2) {
            if ($this->offer == "Yes") {
                $this->weber = $this->second * 15 / 100;
            } else {
                $this->weber = $this->none;
            }
        } elseif ($this->semester == 3) {
            if ($this->offer == "Yes") {
                $this->weber = $this->three * 20 / 100;
            } else {
                $this->weber = $this->none;
            }
        }

        // total
        if ($this->semester == 1) {
            if ($this->offer == "Yes") {
                $this->total = $this->cost - $this->weber;
            } else {
                $this->total = $this->cost;
            }
        } elseif ($this->semester == 2) {
            if ($this->offer == "Yes") {
                $this->total = $this->cost - $this->weber;
            } else {
                $this->total = $this->cost;
            }
        } elseif ($this->semester == 3) {
            if ($this->offer == "Yes") {
                $this->total = $this->cost - $this->weber;
            } else {
                $this->total = $this->cost;
            }
        }

    }

    public function __construct()
    {
        session_start();

        $this->con = new PDO('mysql:host=localhost;dbname=php-26-arafat', "root", "");
    }

    public function validationMessage($sm = "")
    {
        if (isset($_SESSION["$sm"]) && !empty($_SESSION["$sm"])) {
            echo $_SESSION["$sm"];
            unset($_SESSION["$sm"]);
        }
    }

    public function store()
    {

        if (!empty($this->title) && !empty($this->semester)) {


            try {
                $query = "INSERT INTO semester(id, title, semester, offer, unique_id, cost, weber, total)
    VALUES(:i, :t, :s, :o, :uid, :cs, :w, :to)";

                $stmt = $this->con->prepare($query);
                $stmt->execute(array(
                    ':i' => null,
                    ':t' => $this->title,
                    ':s' => $this->semester,
                    ':o' => $this->offer,
                    ':uid' => uniqid(),
                    ':cs' => $this->cost,
                    ':w' => $this->weber,
                    ':to' => $this->total,
                ));

            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
            if ($stmt) {
                $_SESSION['messages'] = "Daa Successfully Submitted";
            }
        } else {
            $_SESSION['N_V'] = "$this->title";
            $_SESSION['S_V'] = "$this->semester";
        }
        header("location:index.php");
    }

    public function index()
    {
        $qr = "SELECT * FROM `semester`";
        $query = $this->con->prepare($qr);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function show()
    {
        $qr = "SELECT * FROM `semester` WHERE unique_id=" . "'" . $this->id . "'";
        $query = $this->con->prepare($qr);
        $query->execute();

        $row = $query->fetch(PDO::FETCH_ASSOC);
        return $row;


    }

    public function update()
    {

        try {
            $query = "UPDATE `semester` SET `title` = '$this->title', `semester` = '$this->semester', `offer` = '$this->offer', `cost` = '$this->cost', `weber` = '$this->weber', `total` = '$this->total' WHERE `semester`.`unique_id`=" . "'" . $this->id . "'";

            $stmt = $this->con->prepare($query);
            $stmt->execute();

            if ($stmt) {
                $_SESSION['messages'] = "Daa Successfully Updated";
            }
            header("location:edit.php?id=$this->id");

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }






        /*try {
            $pdo = $this->con;
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $query = "UPDATE `semester`   
                    SET `title` = :t,
                        `semester` = :s,
                        `offer` = :o,
                        `cost` = :cs,
                        `weber` = :w,
                        `total` = :to
                     WHERE `unique_id` = :uid ";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array(
                ':t' => $this->title,
                ':s' => $this->semester,
                ':o' => $this->offer,
                ':cs' => $this->cost,
                ':w' => $this->weber,
                ':to' => $this->total,
                ':uid' => "'" . $this->id . "'",
            ));
            echo $stmt->rowCount();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }*/

    }

    public function delete()
    {

        try {
            $query = "DELETE FROM `semester` WHERE `semester`.`unique_id`=" . "'" . $this->id . "'";

            $stmt = $this->con->prepare($query);
            $stmt->execute();


            if ($stmt) {
                $_SESSION["messages"] = "Data Successfully Deleted";
            }
            header("location:index.php");

        }catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }


}