<?php
?>

<html>
<head>

    <title>J - table</title>

    <script src="jquery.min.js" type="text/javascript"></script>
    <link href="jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <script src="jquery-ui.min.js" type="text/javascript"></script>

    <!-- Include one of jTable styles. -->
    <link href="themes/metro/blue/jtable.min.css" rel="stylesheet" type="text/css"/>
     
    <!-- Include jTable script file. -->
    <script src="jquery.jtable.min.js" type="text/javascript"></script>
</head>
<body>
<div id="PersonTableContainer"></div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#PersonTableContainer').jtable({
            title: 'Table of people',
            actions: {
                listAction: '/GettingStarted/PersonList',
                createAction: '/GettingStarted/CreatePerson',
                updateAction: '/GettingStarted/UpdatePerson',
                deleteAction: '/GettingStarted/DeletePerson'
            },
            fields: {
                PersonId: {
                    key: true,
                    list: false
                },
                Name: {
                    title: 'Author Name',
                    width: '40%'
                },
                Age: {
                    title: 'Age',
                    width: '20%'
                },
                RecordDate: {
                    title: 'Record date',
                    width: '30%',
                    type: 'date',
                    create: false,
                    edit: false
                }
            }
        });
    });
</script>
</body>
</html>
