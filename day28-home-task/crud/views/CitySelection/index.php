<?php

include_once "../../vendor/autoload.php";

use App\CitySelection\CitySelection;

$obj = new CitySelection();

$alldata = $obj->index();

//echo "<pre>";
//print_r($alldata);
//echo "</pre>";

?>

<html>
<head>
    <title>Index | Page</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top: 50px">
            <h3>Phone,Date,City List Page</h3>
            <a class="btn-link" href="create.php">Back to Create page</a>
            <strong class="text-success"><?php $obj->validationMessage("M_mess") ?></strong>
            
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Date</th>
                    <th>City</th>
                    <th colspan="3" class="text-center">Action</th>
                </tr>
                </thead>

                <tbody>
                <?php
                $serial = 1;
                if (isset($alldata) && !empty($alldata)) {
                    foreach ($alldata as $onedata) { ?>

                        <tr>
                            <td><?php echo $serial++ ?></td>
                            <td><?php echo $onedata['title'] ?></td>
                            <td><?php echo $onedata['phone_number'] ?></td>
                            <td><?php echo $onedata['dates'] ?></td>
                            <td><?php echo $onedata['city'] ?></td>
                            <td><a href="show.php?id=<?php echo $onedata['u_id'] ?>">Show</a></td>
                            <td><a href="edit.php?id=<?php echo $onedata['u_id'] ?>">Edit</a></td>
                            <td><a href="delete.php?id=<?php echo $onedata['u_id'] ?>">Delete</a></td>
                        </tr>

                    <?php }
                } else { ?>
                    <tr>
                        <td class="text-center" colspan="8">No available data</td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
</body>
</html>

