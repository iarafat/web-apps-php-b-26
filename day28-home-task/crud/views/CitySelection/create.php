<?php

include_once "../../vendor/autoload.php";

use App\CitySelection\CitySelection;

$obj = new CitySelection();

//
//$date = date("d-m-Y", strtotime($date));
//
//$unixtime = 1307595105;
//echo $time = date("m-d-Y ",$unixtime);
?>

<html>
<head>
    <title>Create | Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="city-selection-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 m-top">
                <h3>City Selection</h3>
                <a class="btn-link" href="index.php">Back to List</a><br>
                <strong class="text-success"><?php $obj->validationMessage("M_mess") ?></strong>

                <form action="store.php" method="post">
                    <label>Name: </label>
                    <input type="text" name="name" class="form-control" value="<?php $obj->validationMessage("N_VS") ?>">
                    <p class="text-danger"><?php $obj->validationMessage("NE_Mes") ?></p><br>

                    <label>Phone: </label>
                    <input type="number" name="phone" class="form-control" value="<?php $obj->validationMessage("PH_VS") ?>">
                    <p class="text-danger"><?php $obj->validationMessage("PH_Mes") ?></p><br>

                    <label>Entry  Date: </label>
                    <input type="date" name="date" class="form-control">
                    <p class="text-danger"><?php $obj->validationMessage("D_Mes") ?></p><br>

                    <label>Select Your City:  </label>
                    <select name="city" class="form-control">
                        <option selected disabled hidden>Select Here</option>
                        <option value="Chittagong">Chittagong</option>
                        <option value="Dhaka">Dhaka</option>
                        <option value="Barisal">Barisal</option>
                        <option value="Khulna">Khulna</option>
                        <option value="Rajshahi">Rajshahi</option>
                        <option value="Rangpur">Rangpur</option>
                        <option value="Sylhet">Sylhet</option>

                    </select>
                    <p class="text-danger"><?php $obj->validationMessage("C_Mes") ?></p>
                    <br><br>

                    <input type="submit" class="btn btn-block">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
