<?php

include_once "../../vendor/autoload.php";

use App\CitySelection\CitySelection;

$obj = new CitySelection();

$obj->prepare($_GET);

$singledata = $obj->show();

//echo "<pre>";
//print_r($singledata);
//echo "</pre>";

if (isset($singledata) && !empty($singledata)){

?>


<html>
<head>
    <title>Index | Page</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top: 50px">
            <h3>Phone,Date,City List Page</h3>
            <a class="btn-link" href="create.php">Back to Create page</a>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Date</th>
                    <th>City</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td><?php echo $singledata['id'] ?></td>
                    <td><?php echo $singledata['title'] ?></td>
                    <td><?php echo $singledata['phone_number'] ?></td>
                    <td><?php echo $singledata['dates'] ?></td>
                    <td><?php echo $singledata['city'] ?></td>
                    <td><a href="edit.php?id=<?php echo $singledata['u_id'] ?>">Edit</a></td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
</body>
</html>
<?php
} else {
    $_SESSION['Errors'] = "ID not found :(";
    header("location:errors.php");
}

    ?>