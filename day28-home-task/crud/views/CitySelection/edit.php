<?php

include_once "../../vendor/autoload.php";

use App\CitySelection\CitySelection;

$obj = new CitySelection();

$obj->prepare($_GET);

$singledata = $obj->show();


//echo "<pre>";
//print_r($singledata);
//echo "</pre>";
if (isset($singledata) && !empty($singledata)){

?>


<html>
<head>
    <title>Edit | Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="city-selection-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 m-top">
                <h3>City Selection</h3>
                <a class="btn-link" href="index.php">Back to List</a><br>
                <strong class="text-success"><?php $obj->validationMessage("M_mess") ?></strong>

                <form action="update.php" method="post">
                    <label>Name: </label>
                    <input type="text" name="name" class="form-control" value="<?php echo $singledata['title']?>"><br>

                    <label>Phone: </label>
                    <input type="number" name="phone" class="form-control" value="<?php echo $singledata['phone_number']?>"><br>

                    <label>Entry  Date: </label>
                    <input type="date" name="date" class="form-control" value="<?php echo $singledata['dates']?>"><br>

                    <label>Select Your City:  </label>
                    <select name="city" class="form-control">
                        <option value="Chittagong" <?php echo ($singledata['city'] == "Chittagong") ? 'selected' : ''; ?> >Chittagong</option>
                        <option value="Dhaka" <?php echo ($singledata['city'] == "Dhaka") ? 'selected' : ''; ?> >Dhaka</option>
                        <option value="Barisal" <?php echo ($singledata['city'] == "Barisal") ? 'selected' : ''; ?> >Barisal</option>
                        <option value="Khulna" <?php echo ($singledata['city'] == "Khulna") ? 'selected' : ''; ?> >Khulna</option>
                        <option value="Rajshahi" <?php echo ($singledata['city'] == "Rajshahi") ? 'selected' : ''; ?> >Rajshahi</option>
                        <option value="Rangpur" <?php echo ($singledata['city'] == "Rangpur") ? 'selected' : ''; ?> >Rangpur</option>
                        <option value="Sylhet" <?php echo ($singledata['city'] == "Sylhet") ? 'selected' : ''; ?> >Sylhet</option>

                    </select>
                    <br><br>
                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                    <input type="submit" class="btn btn-block">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<?php
} else {
    $_SESSION['Errors'] = "ID not found :(";
    header("location:errors.php");
}

?>