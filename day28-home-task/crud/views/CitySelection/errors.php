
<html>
<head>
    <title>Errors | Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<?php
session_start();

if (isset($_SESSION['Errors'])){
    echo "<h2 class='text-danger'>".$_SESSION['Errors']."</h2>";
    unset($_SESSION['Errors']);
}
?>
<a href="index.php">Back to list.</a>

</body>
</html>
