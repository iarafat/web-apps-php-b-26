<?php

namespace App\CitySelection;

use PDO;


class CitySelection
{
    public $id = '';
    public $title = '';
    public $phone = '';
    public $date = '';
    public $city = '';
    public $data = '';

    public $con = '';


    public function prepare($data = '')
    {

        // validation
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['name'])) {

            $this->title = $data['name'];

        } else {

            $_SESSION['NE_Mes'] = "Name Required *";
        }

        if (!empty($data['phone'])) {

            $this->phone = $data['phone'];

        } else {

            $_SESSION['PH_Mes'] = "Phone number Required *";
        }

        if (!empty($data['date'])) {

            $this->date = date("Y-m-d", strtotime($data['date']));

        } else {

            $_SESSION['D_Mes'] = "Date Required *";
        }

        if (!empty($data['city'])) {

            $this->city = $data['city'];
        } else {
            $_SESSION['C_Mes'] = "City Required *";
        }


    }

    public function __construct()
    {
        session_start();

        $this->con = new PDO('mysql:host=localhost;dbname=php-26-arafat', "root", "");
    }

    public function validationMessage($sm = "")
    {
        if (isset($_SESSION["$sm"]) && !empty($_SESSION["$sm"])) {
            echo $_SESSION["$sm"];
            unset($_SESSION["$sm"]);
        }
    }

    public function store()
    {
        if (!empty($this->title) && !empty($this->date) && !empty($this->city) && !empty($this->phone)) {
            try {
                $query = "INSERT INTO city_selection(id, title, phone_number, dates, city, u_id) VALUES(:i, :t, :ph, :d, :ci, :uid)";

                $stmt = $this->con->prepare($query);
                $stmt->execute(array(
                    ':i' => null,
                    ':t' => $this->title,
                    ':ph' => $this->phone,
                    ':d' => $this->date,
                    ':ci' => $this->city,
                    ':uid' => uniqid(),
                ));
                if ($stmt) {
                    $_SESSION['M_mess'] = "Submitted";
                }

            } catch (PDOException $e) {
                echo "Errors: " . $e->getMessage();
            }
        } else {
            $_SESSION['N_VS'] = $this->title;
            $_SESSION['PH_VS'] = $this->phone;
        }
        header("location:index.php");
    }

    public function index()
    {
        $qr = "SELECT * FROM city_selection";
        $query = $this->con->prepare($qr);
        $query->execute();

        $row = $query->fetchAll();
        return $row;
    }

    public function show()
    {
        $qr = "SELECT * FROM city_selection WHERE u_id=" . "'" . $this->id . "'";
        $query = $this->con->prepare($qr);
        $query->execute();

        $row = $query->fetch();
        return $row;
    }

    public function update()
    {
        try {
            $query = "UPDATE city_selection SET title = :t, phone_number = :ph, dates = :d, city = :ci WHERE u_id = :uid";
            $stmt = $this->con->prepare($query);
            $stmt->execute(array(
                ':t' => $this->title,
                ':ph' => $this->phone,
                ':d' => $this->date,
                ':ci' => $this->city,
                ':uid' => $this->id,
            ));
            if ($stmt) {
                $_SESSION['M_mess'] = "Updated";
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        header("location:edit.php?id=$this->id");
    }

    public function delete()
    {
        try {
            $query = "DELETE FROM city_selection WHERE u_id = :uid";
            $stmt = $this->con->prepare($query);
            $stmt->bindParam(':uid', $this->id);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['M_mess'] = "Deleted";
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        header("location:index.php");
    }
}

?>