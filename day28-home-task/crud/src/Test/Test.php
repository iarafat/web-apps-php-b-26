<?php

namespace App\Test;


use PDO;


class Test
{
    public $id = '';
    public $title = '';
    public $data = '';

    public $con = '';


    public function prepare($data = '')
    {

        // validation
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['name'])) {

            $this->title = $data['name'];

        } else {

            $_SESSION['NE_Mes'] = "Name Required *";
        }

    }

    public function __construct()
    {
        session_start();

        $this->con = new PDO('mysql:host=localhost;dbname=tests', "root", "");
    }

    public function validationMessage($sm = "")
    {
        if (isset($_SESSION["$sm"]) && !empty($_SESSION["$sm"])) {
            echo $_SESSION["$sm"];
            unset($_SESSION["$sm"]);
        }
    }

    public function store()
    {

        if (!empty($this->title)) {


            try {
                $query = "INSERT INTO testing(id, title)
    VALUES(:i, :t)";

                $stmt = $this->con->prepare($query);
                $stmt->execute(array(
                    ':i' => null,
                    ':t' => $this->title,
                ));

            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
            if ($stmt) {
                $_SESSION['messages'] = "Daa Successfully Submitted";
            }
        } else {
            $_SESSION['N_V'] = "$this->title";
        }
        header("location:index.php");
    }

//
//    public function store()
//    {
//        try {
//            $query = "INSERT INTO `testing`(`id`, `title`) VALUES (?, ?);";
//
//            $stmt = $this->con->prepare($query);
//            $stmt->execute(array(null,$this->title,));
//        } catch (PDOException $e) {
//            echo 'Error: ' . $e->getMessage();
//        }
//
//    }


}