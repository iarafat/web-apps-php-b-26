<?php

class MyClass{
    public $prop1 = "I'm a class property!";

    public function setProperty($newval){
        $this->prop1 = $newval;
    }

    public function getProperty(){
        return $this->prop1."<br>";
    }
}
$obj = new MyClass();
$obj2 = new MyClass();
//var_dump($obj);

//echo $obj->prop1;

echo $obj->getProperty();
echo $obj2->getProperty();

$obj->setProperty("I'm a new property value!!");
$obj2->setProperty("I belong to the second instance!!!");

echo $obj->getProperty();
echo $obj2->getProperty();
?>