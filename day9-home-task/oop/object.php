<?php

class foo {
    function do_foo(){
        echo "Doing foo.";
    }
}

$bar = new foo;
$bar->do_foo();

echo "<br>";
echo "<br>";

$obj = (object) array('1'=>'foo');
var_dump(isset($obj->{'1'}));
echo "<br>";
var_dump(key($obj));
echo "<br>";
echo "<br>";

$obje = (object)'ciao';
echo $obje->scalar;

?>