<?php

class simpleClass {

    public $var = "a default value";

    public function displayVar(){
        echo $this->var;
    }
}

$a = new simpleClass();
$a->displayVar();
echo "<br>";
echo "<br>";
echo "<br>";

class A {
    function foo(){
        if (isset($this)){
            echo '$this is defined (';
            echo get_class($this);
            echo ")<br>";
        } else {
            echo "\$this is not defined<br>";
        }
    }
}

class B {
    function bar(){
        A::foo();
    }
}

$b = new A();
$b->foo();

A::foo();

$c = new B();
$c->bar();

B::bar();

echo "<br>";
echo "<br>";
echo "<br>";


/*class Books {
    var $price;
    var $title;

    function setPrice($par){
        $this->price = $par;
    }

    function getPrice(){
        echo $this->price."<br>";
    }

    function setTitle($par){
        $this->title = $par;
    }

    function getTitle(){
        echo $this->title."<br>";
    }
}

$physics = new Books();
$chemistry = new Books();
$maths = new Books();

$physics->setTitle("Physics for high school");
$chemistry->setTitle("Advanced Chemistry");
$maths->setTitle("Algebra");

$physics->setPrice(10);
$chemistry->setPrice(11);
$maths->setPrice(12);

$physics->getTitle();
$chemistry->getTitle();
$maths->getTitle();

$physics->getPrice();
$chemistry->getPrice();
$maths->getPrice();

*/

echo "<br>";
echo "<br>";
echo "<br>";

class Book {

    function __construct($par1, $par2)
    {
        $this->price = $par1;
        $this->title = $par2;
    }

    function setPrice($par1){
        $this->price = $par1;
    }

    function getPrice(){
        echo $this->price."<br>";
    }

    function setTitle($par2){
        $this->title = $par2;
    }

    function getTitle(){
        echo $this->title."<br>";
    }

}

$physics = new Book("Physics for high school", 10);
$chemistry = new Book("Chemistry for high school", 11);
$maths = new Book("Maths for high school", 12);

$physics->getTitle();
$chemistry->getTitle();
$maths->getTitle();

$physics->getPrice();
$chemistry->getPrice();
$maths->getPrice();

?>