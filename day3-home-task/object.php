<?php
// Object

class Car {
    function Car(){
        $this->model = "BMW";
    }
}

// create an object
$obj = new Car();

// show object properties
echo $obj->model;

?>