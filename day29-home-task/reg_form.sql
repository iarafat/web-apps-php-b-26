-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2016 at 10:48 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php-26-arafat`
--

-- --------------------------------------------------------

--
-- Table structure for table `reg_form`
--

CREATE TABLE `reg_form` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `verification_id` varchar(255) NOT NULL,
  `user_name` varchar(225) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `restore` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reg_form`
--

INSERT INTO `reg_form` (`id`, `unique_id`, `verification_id`, `user_name`, `password`, `email`, `is_active`, `is_admin`, `is_delete`, `created`, `modified`, `deleted`, `restore`) VALUES
(36, '57a0e039873aa', '57a0e039873aa', 'iyasin2', '369258147', 'mdiyasin12@gmail.com', 1, 1, 0, '2016-08-03 12:02:33', '2016-08-03 11:22:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '57a17fd92bfe2', '57a17fd92bfe2', 'arafat2', '123456', 'mdiyasin@gmail.com', 1, 0, 0, '2016-08-03 11:23:37', '2016-08-03 11:24:44', '2016-08-03 11:25:28', '2016-08-03 11:26:13'),
(40, '57a1bd5d493d4', '57a1bd5d493d4', 'iyasin', '147147', 'miyasinarafat2@gmail.com', 1, 0, 1, '2016-08-03 03:46:05', '0000-00-00 00:00:00', '2016-08-03 03:55:27', '0000-00-00 00:00:00'),
(41, '57a229f36dfb6', '57a229f36dfb6', 'ghjnerasgk', '123456', 'mdiyasin12@gmail.com', 0, 0, 0, '2016-08-03 11:29:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '57a33057341f2', '57a33057341f2', 'iyasinarafat', '123456', 'miyasinarafat@gmail.com', 0, 0, 0, '2016-08-04 06:08:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '57a5d81b3e7e6', '57a5d81b3e7e6', 'abcabc', 'abcabc', 'abcabc@abc.com', 1, 0, 0, '2016-08-06 06:29:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '57a64b0d51ced', '57a64b0d51ced', '123456', '123456', '123456@mail.com', 1, 0, 0, '2016-08-07 02:39:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reg_form`
--
ALTER TABLE `reg_form`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reg_form`
--
ALTER TABLE `reg_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
