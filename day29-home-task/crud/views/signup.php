<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SinUp Form</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<!--contact-form-->
<?php include_once "include/navbar.php"; ?>
<div class="contact-form">

    <div class="container">

        <div class="row">
            <div class="col-md-5 col-md-offset-3">
                <h3>SignUp Form</h3>
                <strong class="text-danger">
                    <?php $obj->Validation("Is_D"); ?>
                </strong><br>
                <form action="signup-process.php" method="post">
                    <label>User Name</label>
                    <input class="form-control" type="text" name="usname" value="<?php $obj->Validation("N_va"); ?>">
                    <p class="text-danger"><?php $obj->Validation("US_N"); ?></p><br>

                    <label>Password</label>
                    <input class="form-control" type="password" name="password" value="<?php $obj->Validation("P_va"); ?>">
                    <p class="text-danger"><?php $obj->Validation("US_P"); ?></p><br>

                    <label>Re-Password</label>
                    <input class="form-control" type="password" name="re-password" value="<?php $obj->Validation("RP_va"); ?>">
                    <p class="text-danger"><?php $obj->Validation("US_PM"); ?></p><br>

                    <label>Email</label>
                    <input class="form-control" type="email" name="email"  value="<?php $obj->Validation("E_va"); ?>">
                    <p class="text-danger"><?php $obj->Validation("U_EM"); ?></p><br>

                    <input class="btn btn-block" type="submit" value="Send" name="submitted">
                </form>
            </div>
        </div>
    </div>
</div>
<!--/contact-form-->
</body>
</html>