<?php
$hostname = "localhost";
$username = "root";
$password = "";
$databaseName = "test_js";

$connect = mysqli_connect($hostname, $username, $password, $databaseName);
$query = "SELECT * FROM `tc_tuto_pagination`";
$result = mysqli_query($connect, $query);
    ?>

    <html>
    <head>
        <title>Index | Page</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/footable.min.js"></script>
        <script src="js/footable.paginate.min.js"></script>
        <script>

        </script>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top: 50px">
                <table class="footable table table-bordered table-hover mtop" data-page-size="10" data-next-text="Next" data-previous-text="Previous">
                    <thead>
                    <tr>
                        <!-- data-toggle = where the toggle icon will appear -->

                        <th>Id</th>
                        <!-- hide this column in mobile -->
                        <th>title</th>
                        <!-- hide this column in tablet and mobile -->
                        <th>Dis</th>
                    </tr>
                    </thead>
                    <tbody>
                    <!-- populate table from mysql database -->
                    <?php while($row1 = mysqli_fetch_array($result)):;?>
                        <tr>
                            <td><?php echo $row1[0];?></td>
                            <td><?php echo $row1[1];?></td>
                            <td><?php echo $row1[2];?></td>
                        </tr>
                    <?php endwhile;?>
                    </tbody>
                    <!-- the pagination -->
                    <!-- hide-if-no-paging = hide the pagination control -->
                    <tfoot class="hide-if-no-paging">
                    <td colspan="3">
                        <div class="pagination text-center"></div>
                    </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.footable').footable({
//                "paging": {
//                    "enabled": true
//                },
//                "filtering": {
//                    "enabled": true
//                },
//                "sorting": {
//                    "enabled": true
//                }
            });
        });

    </script>
    </body>
    </html>
