<?php

include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();

if ($_SERVER["REQUEST_METHOD"] == "POST") {


//    echo "<pre>";
    //print_r($_POST);
    //print_r($_FILES);
    if (!empty($_FILES['photo']['name'])) {
        $photo_name = time() . uniqid() . $_FILES['photo']['name'];
        $photo_type = $_FILES['photo']['type'];
        $photo_tmp_name = $_FILES['photo']['tmp_name'];
        $photo_size = $_FILES['photo']['size'];
        $photo_extension = strtolower(end(explode('.', $photo_name)));
        //print_r($photo_extension);
        $required_format = array('jpg', 'jpeg', 'png', 'gif');
        if (in_array($photo_extension, $required_format) === false) {
            $_SESSION['photo_extension'] = "Invalid file format";
        } elseif ($photo_size > 2000000) {
            $_SESSION['photo_size'] = "Image size too large";
        } else {
            move_uploaded_file($photo_tmp_name, 'photo/' . $photo_name);
            $_POST['photo'] = $photo_name;
        }

    }

    $obj->prepare($_POST);
    $obj->Verification();
    $obj->profileUpdate();

    /*  if (isset($_FILES['photo']['name'])) {
          if (!$_FILES['photo']['error']) {
              $new_file_name = strtolower($_FILES['photo']['name']); //rename file
              if ($_FILES['photo']['size'] > (1024000)) //can't be larger than 1 MB
              {
                  $_SESSION['profile_pic'] = 'Oops!  Your file\'s size is to large.';
              }
              move_uploaded_file($_FILES['photo']['tmp_name'], 'photo/' . $new_file_name);
              $con = mysql_connect("localhost", "root", "");
              mysql_select_db("php-26-arafat");
              $q = mysql_query("UPDATE profiles SET profile_pic = '" . $_FILES['photo']['name'] . "' WHERE profiles.users_id= '" . $_SESSION['Login_data']['id'] . "'");

          } else {
              $_SESSION['profile_pic'] = 'Ooops!  Your upload triggered the following error:  ' . $_FILES['photo']['error'];
          }
      }*/

} else {
    $_SESSION['Errors_R'] = "404 not found :(";
    header("location:errors.php");
}

?>