<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<!--contact-form-->
<?php include_once "include/navbar.php"; ?>
<div class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-3">

                <h3>Login Form</h3>
                <strong class="text-success">
                    <?php $obj->Validation("R_messages"); ?>
                    <?php $obj->Validation("Logout_M"); ?>
                    <?php $obj->Validation("R_C"); ?>
                    <?php $obj->Validation("confirm"); ?>
                </strong>
                <strong class="text-danger">
                    <?php $obj->Validation("EP_M"); ?>
                    <?php $obj->Validation("E_active"); ?>
                    <?php $obj->Validation("R_Login"); ?>
                </strong><br>

                <form action="login-process.php" method="post">

                    <label>Email</label>
                    <input class="form-control" type="email" name="logemail" value="<?php $obj->Validation("Log_EV"); ?>">
                    <p class="text-danger"><?php $obj->Validation("Log_EM"); ?></p><br>

                    <label>Password</label>
                    <input class="form-control" type="password" name="logpassword" value="<?php $obj->Validation("Log_PV"); ?>"><br>
                    <p class="text-danger"><?php $obj->Validation("Log_P"); ?></p><br>


                    <input class="btn btn-block" type="submit" value="Login">
                </form>
            </div>
        </div>
    </div>
</div>
<!--/contact-form-->
</body>
</html>
