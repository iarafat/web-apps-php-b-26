<?php
namespace App\Bitm\SEIP124286\Registration;
use PDO;

class Registration
{
    public $id = '';
    public $user_name = '';
    public $password = '';
    public $repeat_password = '';
    public $email = '';
    public $varification_id = '';
    public $is_active = '';
    public $is_admin = '';
    public $created = '';
    public $modified = '';
    public $deleted = '';
    public $data = '';
    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = '';
    public $usrname = '';
    public $logusername = '';
    public $logpassword = '';


    public function __construct()
    {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->conn = new PDO('mysql:host=localhost;dbname=registration', $this->dbuser, $this->dbpass);
        // $conn =  mysql_connect('localhost','root','')Or die("OPPS! Connected Fail");
        //$db = mysql_select_db('php-26') or die("Database connect fail");


    }


    public function prepare($data = '')
    {


        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }


        if(!empty($data['username'])){
            $this->user_name = $data['username'];
        }

        if(!empty($data['password'])){
            $this->password = $data['password'];
        }

        if(!empty($data['re_pass'])){
            $this->repeat_password = $data['re_pass'];
        }

        if(!empty($data['email'])){
            $this->email = $data['email'];
        }

        if(!empty($data['logusername'])){
            $this->logusername = $data['logusername'];
        }


        if(!empty($data['logpassword'])){
            $this->logpassword = $data['logpassword'];
        }


    }


    public function store()
    {

        if (!empty($this->user_name) && !empty($this->password) && !empty($this->repeat_password) && !empty($this->email)) {

            if ($this->error == FALSE) {
                try {
                    $query = "INSERT INTO td_registration (id, unique_id, verification_id, username, password, email, is_active, is_admin, created, modified, deleted) VALUES (:id, :unique_id, :verification_id, :username, :password, :email, :is_active, :is_admin, :created, :modified, :deleted)";
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute(array(
                        ':id' => null,
                        ':unique_id' => uniqid(),
                        ':verification_id' => uniqid(),
                        ':username' => $this->user_name,
                        ':password' => $this->password,
                        ':email' => $this->email,
                        ':is_active' => 0, //$this->is_active,
                        ':is_admin' => 0, //$this->is_admin,
                        ':created' => date("Y-m-d h:i:s"),
                        ':modified' => '', //$this->deleted
                        ':deleted' => 0, //$this->deleted
                    ));
                    header('location:create.php');
                    $_SESSION['message'] = "You are successfully submited";
                } catch (PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            } else {
                header('location:create.php');
            }
        }
    }

    //error massege minimizing method
    public function errHandling($errors = '')
    {
        if (isset($_SESSION["$errors"]) && !empty($_SESSION["$errors"])) {
            echo $_SESSION["$errors"];
            unset($_SESSION["$errors"]);
        }
    }

/*    public function verify()
    {
        $verificationcode = "'" . $_GET['vid'] . "'";
        $selectQuery = "SELET * FROM `td_registration` WHERE `verification_id` = $verificationcode";
        $stmt = $this->conn->prepare($selectQuery);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (empty($row['vefification_id'])) {
            $_SESSION['err_msg'] = "Invalid Registration";
            header('location:error.php');

        } else {
            if ($row['is_active'] = 1) {
                $_SESSION['err_msg'] = 'Email Already verified. Go to <a href="login.php">login page</a>.';
                header('location:error.php');
            } else {
                try {
                    $updateQuery = "UPDATE `td_registration` SET `is_active` = '1' WHERE `td_registration`.`verification_id` = $verificationcode";
                    $stmt = $this->conn->prepare($updateQuery);
                    $stmt->execute();
                    $_SESSION['verifiedMsg'] = "Registration Process Completed. Now Login.";
                    header('location:login.php');

                } catch (PDOException $e) {
                    echo 'error' . $e->getMessage();
                }
            }
        }

    }*/


    public function login()
    {

        $selectQuery = "SELECT * FROM td_registration WHERE username = '$this->logusername' AND password = '$this->logpassword'";

        $stmt = $this->conn->prepare($selectQuery);
        $stmt->execute();
        $user = $stmt->fetch();


        if (isset($user) && !empty($user)) {
            if ($user['is_active'] == 0) {
                $_SESSION['Message'] = "<h3>Your account not verified yet. Check your email and verify</h3>";
                header('location:login.php');
            } else {
                $_SESSION['user'] = $user;
                header('location:dashbord.php');
            }
        } else {
            $_SESSION['Message'] = "<h3>invalid username or password</h3>";
            header('location:login.php');
        }
    }


}

?>


