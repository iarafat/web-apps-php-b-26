<?php 
//include_once '../../../../Src/Bitm/SEIP124286/Registration/Registration.php';
include_once '../../../../vendor/autoload.php';
use  App\Bitm\SEIP124286\Registration\Registration;
$obj = new Registration();

if (isset($_SESSION['verifiedMsg']) && !empty($_SESSION['verifiedMsg'])) {
    echo $_SESSION['verifiedMsg'];
    unset($_SESSION['verifiedMsg']);
}

if (isset($_SESSION['errMsg']) && !empty($_SESSION['errMsg'])) {
    echo $_SESSION['errMsg'];
    unset($_SESSION['errMsg']);
}

?>


<!DOCTYPE html>
<html>
    <head>
        <title>Registration Form</title>
        <style>
            .sin_form{
                background: #ddd;
                min-height: 300px;
                height: auto;
                width: 600px;
                margin: 100px auto;
                
            }
            input[type='text'], input[type='password'], input[type='email']{
                height: 25px; 
                margin-left: 50px;
                overflow:hidden;
               
            }
            label{
                margin-left: 50px;
            }
            input[type='submit']{
                margin-left: 255px;
            }
            .linkpage{
                height: 50px;
                width: 100%;
                background: #efefef;
            }
            .linkpage a{
                display: inline-block;
                text-decoration: none;
                font-family: arial;
                font-size: 16px;
                font-weight: normal;
                color:#999;
                padding-left:20px;  
                text-align: center;
                line-height: 50px;
                
            }
        </style>
    </head>
    <body>
        <div class="sin_form">
            <div class="linkpage">
                   <a href="create.php">Signup</a>
                   <a href="login.php">Login</a>
                  <?php
                        if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
                            echo $_SESSION['Message'];
                            unset($_SESSION['Message']);
                        }
                  ?>
            </div>
            
               
              <a href="index.php">See List</a> ||
            
         
              <form action="loginProcess.php" method="POST">
                        <br>
                        <label> User Name </label><br>
                        <input type="text" name="logusername" size="40" ><?php $obj->errHandling('errName'); ?><br>
                        <label>Password</label><br>
                        <input type="password" name="logpassword" size="40"><?php $obj->errHandling('errPassword'); ?>
                        <br>
                        <br><br>
                        <input type="submit" value="Login">


                    </form>
    
            </div>
    </body>
</html>