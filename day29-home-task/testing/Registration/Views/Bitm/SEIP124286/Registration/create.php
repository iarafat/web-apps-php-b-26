<?php
      session_start();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Registration Form</title>
        <style>
            .sin_form{
                background: #ddd;
                min-height: 400px;
                height: auto;
                width: 600px;
                margin: 100px auto;
                
            }
            input[type='text'], input[type='password'], input[type='email']{
                height: 25px; 
                margin-left: 50px;
                overflow:hidden;
               
            }
            label{
                margin-left: 50px;
            }
            input[type='submit']{
                margin-left: 255px;
            }
            .linkpage{
                height: 50px;
                width: 100%;
                background: #efefef;
            }
            .linkpage a{
                display: inline-block;
                text-decoration: none;
                font-family: arial;
                font-size: 16px;
                font-weight: normal;
                color:#999;
                padding-left:20px;  
                text-align: center;
                line-height: 50px;
                
            }
        </style>
    </head>
    <body>
        <div class="sin_form">
            <div class="linkpage">
                    <a href="create.php">Signup</a>
                   <a href="login.php">Login</a>
                 
            </div>
            
                <?php
                if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
                    echo "<h1 style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['message']."</h1>";
                    unset($_SESSION['message']);
                }
              ?>
              <a href="index.php">See List</a> 
              
            <form action="store.php" method="POST">
                        <br>
                        <label> User Name </label><br>
                        <input type="text" name="username" size="40" value="<?php
                        if (isset($_SESSION['valu_username']) && !empty($_SESSION['valu_username'])) {
                        echo  $_SESSION['valu_username'];
                        unset($_SESSION['valu_username']);
                    }
                     ?>"><?php
                        if (isset($_SESSION['username_unike']) && !empty($_SESSION['username_unike'])) {
                        echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['username_unike']."</span>";
                        unset($_SESSION['username_unike']);
                    }
                     ?><?php
                        if (isset($_SESSION['user_name']) && !empty($_SESSION['user_name'])) {
                        echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['user_name']."</span>";
                        unset($_SESSION['user_name']);
                    }
                     ?><?php
                        if (isset($_SESSION['length']) && !empty($_SESSION['length'])) {
                        echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['length']."</span>";
                        unset($_SESSION['length']);
                    }
                     ?><br>
                        <label>Password</label><br>
                        <input type="password" name="password" size="40"><?php
                        if (isset($_SESSION['password']) && !empty($_SESSION['password'])) {
                        echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['password']."</span>";
                        unset($_SESSION['password']);
                    }
                     ?><?php
                        if (isset($_SESSION['not_match']) && !empty($_SESSION['not_match'])) {
                        echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['not_match']."</span>";
                        unset($_SESSION['not_match']);
                    }
                     ?><?php
                        if (isset($_SESSION['pass_length']) && !empty($_SESSION['pass_length'])) {
                        echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['pass_length']."</span>";
                        unset($_SESSION['pass_length']);
                    }
                     ?>
                        <br>
                        <label>Retype Password</label><br>
                        <input type="password" name="re_pass" size="40"><?php
                        if (isset($_SESSION['repeat_password']) && !empty($_SESSION['repeat_password'])) {
                        echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['repeat_password']."</span>";
                        unset($_SESSION['repeat_password']);
                    }
                     ?><br>
                        <label>Email</label><br>
                        <input type="email" name="email" size="40" value="<?php
                        if (isset($_SESSION['val_email']) && !empty($_SESSION['val_email'])) {
                         echo  $_SESSION['val_email'];
                        unset($_SESSION['val_email']);
                    }
                     ?>"><?php
                        if (isset($_SESSION['email']) && !empty($_SESSION['email'])) {
                         echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['email']."</span>";
                        unset($_SESSION['email']);
                    }
                     ?><?php
                        if (isset($_SESSION['val_email_valid']) && !empty($_SESSION['val_email_valid'])) {
                         echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['val_email_valid']."</span>";
                        unset($_SESSION['val_email_valid']);
                    }
                     ?><?php
                        if (isset($_SESSION['val_email_unike']) && !empty($_SESSION['val_email_unike'])) {
                         echo "<span style='color:green; font-family:tahoma; font-size:14px;'>". $_SESSION['val_email_unike']."</span>";
                        unset($_SESSION['val_email_unike']);
                    }
                     ?><br><br>
                        <input type="submit" value="Submit">


                    </form>
    
            </div>
    </body>
</html>