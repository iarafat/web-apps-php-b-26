<?php
//include_once '../../../../Src/Bitm/SEIP124286/Registration/Registration.php';
include_once '../../../../vendor/autoload.php';
use  App\Bitm\SEIP124286\Registration\Registration;

$obj = new Registration();

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $obj->prepare($_POST);
    $obj->login();
    
}else{
    $_SESSION['errMsg'] = "Invalid location";
    header('location:login.php');
}

?>
