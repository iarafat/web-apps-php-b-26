-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2016 at 10:48 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php-26-arafat`
--

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `father_name` varchar(225) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `phone` int(35) NOT NULL,
  `fax_number` int(35) NOT NULL,
  `web_address` varchar(255) NOT NULL,
  `dateofbirth` date NOT NULL,
  `height` varchar(255) NOT NULL,
  `occupation` varchar(255) NOT NULL,
  `education_status` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `current_job` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `interested` varchar(255) NOT NULL,
  `bio` varchar(255) NOT NULL,
  `nid` int(35) NOT NULL,
  `passport_number` int(35) NOT NULL,
  `skills_area` varchar(255) NOT NULL,
  `language_area` varchar(255) NOT NULL,
  `Blood_group` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `others` varchar(255) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `users_id`, `full_name`, `father_name`, `mother_name`, `gender`, `phone`, `fax_number`, `web_address`, `dateofbirth`, `height`, `occupation`, `education_status`, `religion`, `marital_status`, `current_job`, `nationality`, `interested`, `bio`, `nid`, `passport_number`, `skills_area`, `language_area`, `Blood_group`, `address`, `others`, `profile_pic`, `created`, `modified`, `deleted`) VALUES
(1, 44, 'MD Iyasin Arafat', 'Md farud mia', 'nurjahan begum', 'Male', 1921875585, 54545454, 'https://www.google.com.bd/', '2016-08-07', '5.7 fit', 'none', 'B. A. Honours 1st Year', 'Islam', 'none', 'none', 'Bangladeshi', '''Reading''', 'iojgrkt', 2147483647, 2147483647, '''Accuracy''', '''English''', 'B+', 'a:5:{s:11:"address_one";s:11:"hbfsasehlwk";s:11:"address_two";s:14:"dganskh idsg h";s:4:"post";s:4:"1310";s:5:"state";s:5:"dhaka";s:4:"city";s:5:"Dhaka";}', 'sdfihdhutewr iudg ds', 'bitm.png', '2016-08-06 06:29:15', '2016-08-07 02:34:13', '0000-00-00 00:00:00'),
(2, 45, 'MD Iyasin Arafat', 'Md farud mia', 'nurjahan begum', 'Male', 1921875585, 2147483647, 'https://www.google.com.bd/', '2016-08-07', '5.7 fit', 'none', 'B. A. Honours 1st Year', 'Islam', 'none', 'none', 'Bangladeshi', '''Reading'',''Traveling''', 'kjnkjdsn kh gkjdsng uihsdj guisdh k hsdiughnds ', 2147483647, 2147483647, '''Accuracy'',''Budgeting'',''Calculating_data''', '''English'',''Bangle''', 'B+', 'a:5:{s:11:"address_one";s:11:"skdhgjiesgn";s:11:"address_two";s:26:"kjhgieruhyeir;u heiug huri";s:4:"post";s:4:"1310";s:5:"state";s:5:"dhaka";s:4:"city";s:5:"Dhaka";}', 'dhgior jsdhgi dfuh udfh gi ', 'profile_pic.jpg', '2016-08-07 02:39:41', '2016-08-07 02:44:08', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
