<html>
<head>
    <title>Errors | Page</title>
    <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['Err'])) {
    echo "<h1 class='text-danger'>" . $_SESSION['Err'] . "</h1>";
    unset($_SESSION['Err']);
}

?>

<a href="index.php" class="btn-link">Back To List</a>

</body>
</html>
