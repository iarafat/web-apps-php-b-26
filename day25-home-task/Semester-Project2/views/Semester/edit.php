<?php
include_once "../../vendor/autoload.php";

use App\Semester\Semester;

$obj = new Semester();

$obj->prepare($_GET);

$ondata = $obj->show();

//print_r($ondata);

if (isset($ondata) && !empty($ondata)) {


    ?>

    <html>
    <head>
        <title>Edit | Page</title>
        <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4" style="margin-top: 50px">
                <h3>Semester</h3>
                <a class="btn-link" href="index.php">Back to List</a><br>
                <strong class="text-success"><?php $obj->validationMessage("messages") ?></strong>
                <form action="update.php" method="post">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name" value="<?php echo $ondata['title'] ?>"><br>

                    <label>Semester 1/2/3</label>
                    <input class="form-control" type="text" name="semester" value="<?php echo $ondata['semester'] ?>"><br>

                    <input type="checkbox" name="offer"
                           value="Yes" <?php echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                    <label>Get offer</label><br><br>

                    <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                    <input class="btn btn-block" type="submit" value="Update">
                </form>
            </div>
        </div>
    </div>
    </body>
    </html>


    <?php

} else {
    $_SESSION['Err'] = "ID Not Found";
    header("location:errors.php");
}

?>


