<?php
include_once "../../vendor/autoload.php";
use App\Semester\Semester;

$obj = new Semester();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $obj->prepare($_POST);
    $obj->store();
} else {
    $_SESSION['Err'] = "404 Not Found";
    header("location:errors.php");
}

//,$_POST['first']=9000,$_POST['second']=11000,$_POST['three']=13000
?>