<?php
include_once "../../vendor/autoload.php";

use App\Semester\Semester;

$obj = new Semester();
//$obj->validationMessage();

?>

<html>
<head>
    <title>Create | Page</title>
    <link rel="shortcut icon" href="http://www.themeyellow.com/assets/images/fav.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="margin-top: 50px">
            <h3>Semester</h3>
            <a class="btn-link" href="index.php">Back to List</a><br>
            <strong class="text-success"><?php $obj->validationMessage("messages") ?></strong>
            <form action="store.php" method="post">
                <label>Name</label>
                <input class="form-control" type="text" name="name" value="<?php $obj->validationMessage("N_V") ?>">
                <p class="text-danger"><?php $obj->validationMessage("NE_Mes") ?></p><br>

                <label>Semester 1/2/3</label>
                <input class="form-control" type="text" name="semester" value="<?php $obj->validationMessage("S_V") ?>">
                <p class="text-danger"><?php $obj->validationMessage("NS_Mes") ?></p><br>


                <input type="checkbox" name="offer" value="Yes">
                <label>Get offer</label><br><br>

                <input type="submit" class="btn btn-block">
            </form>
        </div>
    </div>
</div>
</body>
</html>
