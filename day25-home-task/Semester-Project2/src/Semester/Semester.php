<?php

namespace App\Semester;

class Semester
{
    public $id = '';
    public $title = '';
    public $semester = '';
    public $offer = '';
    public $data = '';

    public $first = 9000;
    public $second = 11000;
    public $three = 13000;
    public $none = 0;
    public $no = "No";

    public $cost = '';
    public $weber = '';
    public $total = '';


    public function prepare($data = '')
    {

        // validation
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['name'])) {

            $this->title = $data['name'];

        } else {

            $_SESSION['NE_Mes'] = "Name Required *";
        }

        if (!empty($data['semester'])) {

            $this->semester = $data['semester'];

        } else {

            $_SESSION['NS_Mes'] = "Semester Required *";
        }

        if (!empty($data['offer'])) {

            $this->offer = $data['offer'];
        } else {
            $this->offer = $this->no;
        }

//        $_SESSION['AllS_Data'] = $_POST;

        // cost
        if ($this->semester == 1) {
            $this->cost = $this->first;
        } elseif ($this->semester == 2) {
            $this->cost = $this->second;
        } else {
            $this->cost = $this->three;
        }

        // weber
        if ($this->semester == 1) {
            if ($this->offer == "Yes") {
                $this->weber = $this->first * 10 / 100;
            } else {
                $this->weber = $this->none;
            }
        } elseif ($this->semester == 2) {
            if ($this->offer == "Yes") {
                $this->weber = $this->second * 15 / 100;
            } else {
                $this->weber = $this->none;
            }
        } elseif ($this->semester == 3) {
            if ($this->offer == "Yes") {
                $this->weber = $this->three * 20 / 100;
            } else {
                $this->weber = $this->none;
            }
        }

        // total
        if ($this->semester == 1) {
            if ($this->offer == "Yes") {
                $this->total = $this->cost - $this->weber;
            } else {
                $this->total = $this->cost;
            }
        } elseif ($this->semester == 2) {
            if ($this->offer == "Yes") {
                $this->total = $this->cost - $this->weber;
            } else {
                $this->total = $this->cost;
            }
        } elseif ($this->semester == 3) {
            if ($this->offer == "Yes") {
                $this->total = $this->cost - $this->weber;
            } else {
                $this->total = $this->cost;
            }
        }

    }

    public function __construct()
    {
        session_start();
        $conn = mysql_connect("localhost", "themeyellow_WNbdTy", "1u!c1wp(CXAI") or die("unable to connect with DB");
        mysql_select_db("themeyellow_php-26-arafat",$conn) or die("unable to connect with Mysql");
    }

    public function validationMessage($sm = "")
    {
        if (isset($_SESSION["$sm"]) && !empty($_SESSION["$sm"])) {
            echo $_SESSION["$sm"];
            unset($_SESSION["$sm"]);
        }
    }

    public function store()
    {
        if (!empty($this->title) && !empty($this->semester)) {
            $query = "INSERT INTO `themeyellow_php-26-arafat`.`semester` (`id`, `title`, `semester`, `offer`, `unique_id`,`cost`,`weber`,`total`) VALUES (NULL, '$this->title', '$this->semester', '$this->offer', '" . uniqid() . "','$this->cost','$this->weber','$this->total')";
            if (mysql_query($query)) {
                $_SESSION['messages'] = "Daa Successfully Submitted";
            }
        } else {
            $_SESSION['N_V'] = "$this->title";
            $_SESSION['S_V'] = "$this->semester";
        }
        header("location:index.php");
    }

    public function index()
    {
        $query = "SELECT * FROM `semester`";
        $mydata = mysql_query($query);
        while ($row = mysql_fetch_assoc($mydata)) {
            $this->data[] = $row;
        }
        return $this->data;
    }

    public function show()
    {
        $query = "SELECT * FROM `semester` WHERE unique_id=" . "'" . $this->id . "'";
        $mydata = mysql_query($query);
        $row = mysql_fetch_assoc($mydata);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `semester` SET `title` = '$this->title', `semester` = '$this->semester', `offer` = '$this->offer', `cost` = '$this->cost', `weber` = '$this->weber', `total` = '$this->total' WHERE `semester`.`unique_id`=" . "'" . $this->id . "'";
//        echo $query;
//        die();
        if (mysql_query($query)) {
            $_SESSION['messages'] = "Data Successfully Updated";
        }
        header("location:edit.php?id=$this->id");
    }

    public function delete()
    {
        $query = "DELETE FROM `semester` WHERE `semester`.`unique_id`="."'".$this->id."'";
        if (mysql_query($query)){
            $_SESSION["messages"] = "Data Successfully Deleted";
        }
        header("location:index.php");
    }


}