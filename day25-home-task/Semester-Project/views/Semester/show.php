<?php
include_once "../../vendor/autoload.php";

use App\Semester\Semester;

$showObject = new Semester();

$showObject->prepare($_GET);

$ondata = $showObject->show();

//print_r($ondata);

if (isset($ondata) && !empty($ondata)) {


    ?>

    <head>
        <title>Index | Page</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2" style="margin-top: 50px">
                <h3>Semester List Page</h3>
                <a class="btn-link" href="create.php">Back to Create page</a> | <a class="btn-link" href="index.php">Back to List page</a>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Semester</th>
                        <th>Offer</th>
                        <th>Cost</th>
                        <th>Weber</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $ondata['id'] ?></td>
                        <td><?php echo $ondata['title'] ?></td>
                        <td><?php echo $ondata['semester'] ?></td>
                        <td><?php echo $ondata['offer'] ?></td>
                        <td><?php echo $ondata['cost'] ?></td>
                        <td><?php echo $ondata['weber'] ?></td>
                        <td><?php echo $ondata['total'] ?></td>
                        <td><a href="edit.php?id=<?php echo $ondata['unique_id'] ?>">Edit</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </body>
    </html>

    <?php

} else {
    $_SESSION['Err'] = "ID Not Found";
    header("location:errors.php");
}

?>

