<?php

// array_rand
$a = array("red","green","blue","yellow","brown");
$random_keys = array_rand($a);

echo "<pre>";
print_r($random_keys);
echo "<br>";
echo "<br>";

// array_replace
$a1 = array("red","green");
$a2 = array("blue","yellow");
print_r(array_replace($a1,$a2));
echo "<br>";
echo "<br>";

// array_replace_recursive
$a3 = array("a"=>array("red"),"b"=>array("green","blue"));
$a4 = array("a"=>array("yellow"),"b"=>array("black"));
print_r(array_replace_recursive($a3,$a4));
echo "<br>";
echo "<br>";

// Differences between array_replace() and array_replace_recursive():
$a5 = array("a"=>array("red"),"b"=>array("green","blue"));
$a6 = array("a"=>array("yellow"),"b"=>array("black"));

$result = array_replace_recursive($a5,$a6);
print_r($result);
echo "<br>";

$result2 = array_replace($a5,$a6);
print_r($result2);
echo "<br>";
echo "<br>";

// array_search
$a7 = array("a" => "red", "b" => "green", "c" => "blue");
echo array_search("red",$a7);
?>