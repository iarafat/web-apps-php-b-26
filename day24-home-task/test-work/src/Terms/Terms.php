<?php

namespace App\Terms;

class Terms
{
    public $id = '';
    public $title = '';
    public $data = '';
    public $terms = '';
    public $email = '';

    public function prepare($data = '')
    {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['name'])) {
            $this->title = $data['name'];
        } else {
            $_SESSION['TN_Mes'] = "Name required";
        }

        if (!empty($data['email'])) {
            $this->email = $data['email'];
        } else {
            $_SESSION['TE_Mes'] = "Email required";
        }

        if (!empty($data['terms'])) {
            $this->terms = $data['terms'];
        } else {
            $_SESSION['TT_Mes'] = "Terms required";
        }

        $_SESSION['AllT_data'] = $_POST;

        return $this;
    }

    public function __construct()
    {
        session_start();
        $conn = mysql_connect('localhost','root', '') or  die('Opps! Unable to connect with Mysql');
        mysql_select_db('') or die('Unable to connect with Database');
    }

    public function store()
    {

    }

    public function index()
    {

    }

    public function show()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }
}