<?php

?>

<html>
<head>
    <title>Terms Form</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="margin-top: 50px">
            <form action="store.php" method="post">
                <label>Name</label>
                <input type="text" name="name" placeholder="Enter Your name" class="form-control"><br>
                <label>Email</label>
                <input type="email" name="email" placeholder="Enter Your email" class="form-control"><br>
                <input type="checkbox" name="terms">
                <label>Check our terms and condition</label>
                <br>
                <br>
                <input type="submit" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
</body>
</html>
