<?php

$color = array('white', 'green', 'red', 'blue', 'black');

list($a, $b, $c, $d, $e) = $color;

echo "The memory of that scene for me is like a frame of film forever frozen at that
	
<br>Moment: the $c carpet, the $b lawn, the $a house, the leaden sky. The new
	
President and his first lady. - Richard M. Nixon"
?>